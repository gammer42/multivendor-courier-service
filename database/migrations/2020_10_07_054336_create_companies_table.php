<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade');
            $table->string('name')->nullable();
            $table->string('owner_name')->nullable();
            $table->string('time_zone')->nullable();
            $table->boolean('cash_on_delivery_cap')->default(false);
            $table->string('currency')->nullable();
            $table->float('delivery_commission', 8, 2)->nullable();
            $table->float('express_commission', 8, 2)->nullable();
            $table->float('cod_commission', 8, 2)->nullable();
            $table->float('return_commission', 8, 2)->nullable();
            $table->float('shipping_commission', 8, 2)->nullable();
            $table->float('admin_commission', 8, 2)->nullable();
            $table->float('admin_payment_remaining', 8, 2)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
