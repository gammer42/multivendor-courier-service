<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade');
            $table->unsignedBigInteger('driver_id')->nullable();
            $table->string('vehicle_type')->nullable();
            $table->string('recipent_phone')->nullable();
            $table->string('recipent_name')->nullable();
            $table->string('recipent_address')->nullable();
            $table->boolean('is_express')->default(false);
            $table->string('status')->nullable();
            $table->string('order_number')->nullable();
            $table->dateTime('status_updated_at')->nullable();
            $table->text('driver_note')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
