<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\User;
use Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::factory(10)->create();
        // Role::create(['name' => 'Super Admin', 'slug' => 'admin']);
        // Role::create(['name' => 'Operator', 'slug' => 'operator']);
        // Role::create(['name' => 'Company', 'slug' => 'company']);
        // Role::create(['name' => 'Driver', 'slug' => 'driver']);
        // Role::create(['name' => 'Store', 'slug' => 'store']);
        // Role::create(['name' => 'Team', 'slug' => 'team']);

        $user = new User();
        $user->name = "Super Admin";
        $user->email = "admin@admin.com";
        $user->password = Hash::make("12345678");
        $user->phone = "1234567890";
        $user->save();

        Role::where('slug', 'admin')->first()->userRoles()->sync($user->id);

    }
}
