<?php

namespace App\Traits;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

trait UserTrait {
    public function userCreate($data)
    {
        // dd($data['name']);
        $user           = new User();
        $user->name     = $data['name'];
        $user->email    = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->phone    = $data['phone'];
        $user->save();
        return $user;
    }

}