<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\UserTrait;

use Illuminate\Support\Facades\Validator;
use App\Models\Driver;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class DriversController extends Controller
{
    use UserTrait;
    public function index()
    {
        $drivers = Driver::with('user')->latest()->paginate(25);
        return response()->json([
            'status' => 'Success',
            'drivers' => $drivers
        ], 200);
    }

    public function show($id)
    {
        $driver = Driver::find($id);
        if (!$driver)
            return response()->json(['status' => 'Driver Not Found'], 404);
        return response()->json([
            'status' => 'Success',
            'driver' => $driver
        ], 200);
    }

    public function store(Request $request)
    {
        // dd($authUserRole = Auth::user()->userRoles[0]->slug);
        $validator = Validator::make($request->all(), [
            'name'          => 'required|string',
            'email'         => 'required|email|unique:users',
            'password'      => 'required|min:8|confirmed',
            'phone'         => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'vehicle_type'  => 'required|string',
            'payment_type'  => 'required|boolean',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()]);
        }

        $data = $request->all();
        $user = $this->userCreate($data);
        if (!$user)
            return response()->json(['status' => 'User Not Create'], 404);

        
        $driver                     = new Driver();
        $driver->user_id            = $user->id;
        $driver->vehicle_type       = $request->vehicle_type;
        $driver->payment_type       = $request->payment_type;
        $driver->salary             = $request->salary;
        $driver->commission_percent = $request->commission_percent;
        $driver->save();

        if (!$driver) {
            $user->delete();
            return response()->json(['status' => 'Driver Not Create'], 404);
        } else {
            Role::where('slug', 'driver')->first()->userRoles()->sync($user->id);
            $authUserRole = Auth::user()->userRoles[0]->slug;
            
            if ($authUserRole == 'operator') {
                $driver->operatorDriver()->sync(Auth::user()->operators->id);
            } else {
                $driver->companyDriver()->sync(Auth::user()->company->id);
            }

            return response()->json([
                'status' => 'Success',
                'user'   => $user,
                'driver' => $driver
            ], 201);
        }
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'vehicle_type'       => 'required|string',
            'payment_type'       => 'required|boolean',
            'salary'             => 'required|numeric',
            'commission_percent' => 'required|numeric',
            'driver_id'          => 'required'
        ]);

        if($validator->fails()){
            return response()->json(['error' => $validator->errors()]);
        }

        $driver = Driver::find($request->driver_id);
        if(!$driver)
            return response()->json(['status' => 'Driver Not Found'], 404);
        $driver->vehicle_type       = $request->vehicle_type;
        $driver->payment_type       = $request->payment_type;
        $driver->salary             = $request->salary;
        $driver->commission_percent = $request->commission_percent;
        $driver->save();
        return response()->json([
            'status' => 'Success',
            'driver' => $driver
        ], 201);
    }

    public function destroy($id)
    {
        $driver = Driver::find($id);
        if (!$driver)
            return response()->json(['status' => 'Driver Not found'], 404);
        User::destroy($driver->user_id);
        $driver->delete();
        return response()->json(['status' => 'Deleted Success'], 200);
    }
}
