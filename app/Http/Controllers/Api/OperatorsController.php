<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Operator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Traits\UserTrait;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Validator;

class OperatorsController extends Controller
{
    use UserTrait;
    public function index()
    {
        $operators = Operator::with('user', 'company')->latest()->paginate(25);
        return response()->json([
            'status'    => 'Success',
            'operators' => $operators
        ], 200);
    }

    public function show($id)
    {
        $operator = Operator::with('user', 'company')->find($id);
        if (!$operator)
            return response()->json(['status' => 'Operator Not found'], 404);
        return response()->json([
            'status'   => 'Success',
            'operator' => $operator
        ], 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'                => 'required|string',
            'email'               => 'required|email|unique:users',
            'password'            => 'required|confirmed',
            'phone'               => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'city'                => 'required|string',
            'country'             => 'required|string',
            'time_zone'           => 'required|timezone',
            'delivery_commission' => 'required|numeric',
            'express_commission'  => 'required|numeric',
            'cod_commission'      => 'required|numeric',
            'return_commission'   => 'required|numeric',
            'shipping_commission' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()]);
        }

        $data = $request->all();
        $user = $this->userCreate($data);
        if (!$user) {
            return response()->json(['status' => 'User Not Create'], 404);
        } else {
            $operator                      = new Operator();
            $operator->user_id             = $user->id;
            $operator->company_id          = Auth::user()->company->id;
            $operator->name                = $request->name;
            $operator->city                = $request->city;
            $operator->country             = $request->country;
            $operator->time_zone           = $request->time_zone;
            $operator->delivery_commission = $request->delivery_commission;
            $operator->express_commission  = $request->express_commission;
            $operator->cod_commission      = $request->cod_commission;
            $operator->return_commission   = $request->return_commission;
            $operator->shipping_commission = $request->shipping_commission;
            $operator->save();

            if ($user && $operator) {
                Role::where('slug', 'operator')->first()->userRoles()->sync($user->id);
                $operator->operatorUser()->sync($user->id);
                return response()->json([
                    'status'   => 'Success',
                    'operator' => $operator,
                    'user'     => $user
                ], 201);
            } else {
                $user->delete();
                return response()->json([
                    'status' => 'Failed',
                ], 403);
            }
        }
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'operator_id'         => 'required',
            'name'                => 'required|string',
            'city'                => 'required|string',
            'country'             => 'required|string',
            'time_zone'           => 'required|timezone',
            'delivery_commission' => 'required|numeric',
            'express_commission'  => 'required|numeric',
            'cod_commission'      => 'required|numeric',
            'return_commission'   => 'required|numeric',
            'shipping_commission' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()]);
        }

        $operator = Operator::find($request->operator_id);
        if (!$operator)
            return response()->json(['status' => 'Operator Not Found'], 404);
        $operator->name                = $request->name;
        $operator->city                = $request->city;
        $operator->country             = $request->country;
        $operator->time_zone           = $request->time_zone;
        $operator->delivery_commission = $request->delivery_commission;
        $operator->express_commission  = $request->express_commission;
        $operator->cod_commission      = $request->cod_commission;
        $operator->return_commission   = $request->return_commission;
        $operator->shipping_commission = $request->shipping_commission;
        $operator->save();
        return response()->json([
            'status'   => 'Success',
            'operator' => $operator
        ], 201);
    }

    public function destroy($id)
    {
        $operator = Operator::find($id);
        if (!$operator)
            return response()->json(['status' => 'Operator Not found'], 404);

        $operator->user->delete();
        $operator->delete();
        return response()->json([
            'status' => 'Deleted Success'
        ], 200);
    }
}
