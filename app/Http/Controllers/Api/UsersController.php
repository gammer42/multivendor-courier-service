<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{

    public function index()
    {
        $users = User::latest()->paginate(25);
        return response()->json([
            'status' => 'Success',
            'users'   => $users
        ], 200);
    }

    public function show($id)
    {
        $user = User::find($id);
        if(!$user)
            return response()->json(['status' => 'User Not Found'], 404);
        return response()->json([
            'status' => 'Success',
            'user'   => $user
        ], 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name'                  => 'required',
            'email'                 => 'required|email|unique:users',
            'password'              => 'required|confirmed',
            'phone'                 => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
        ]);
        if ($validator->fails()) {
            return response()->json([ 'error'=> $validator->errors() ]);
        }
        $data             = $request->all();
        $data['password'] = Hash::make($data['password']);
        $user             = User::create($data);
        return response()->json([
            'status' => 'Success',
            'user'   => $user
        ], 201);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name'                  => 'required',
            'email'                 => 'required|email|unique:users,email,'.$request->user_id,
            'phone'                 => 'required',
            'user_id'               => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([ 'error'=> $validator->errors() ]);
        }

        $user        = User::find($request->user_id);
        if(!$user)
            return response()->json(['status' => 'User Not Found'], 404);
        $user->name  = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->save();

        return response()->json([
            'status' => 'Success',
            'user'   => $user
        ], 201);
    }

    public function destroy($id)
    {
        $user = User::find($id);
        if(!$user)
            return response()->json(['status' => 'User Not Found'], 404);
        $user->delete();
        return response()->json([
            'status' => 'Deleted Success',
        ], 200);
    }

    public function activeUser($id)
    {
        $user = User::find($id);
        if(!$user)
            return response()->json(['status' => 'User Not Found'], 404);
        $user->status = 1;
        $user->save();
        return response()->json([
            'status' => 'Active Success',
        ], 200);
    }

    public function inactiveUser($id)
    {
        $user = User::find($id);
        if(!$user)
            return response()->json(['status' => 'User Not Found'], 404);
        $user->status = 0;
        $user->save();
        return response()->json([
            'status' => 'Inactive Success',
        ], 200);
    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'old_password'      => ['required', 'string', 'min:8'],
            'new_password'      => ['required', 'string', 'min:8'],
            'confirm_password'  => ['required', 'string', 'min:8'],
        ]);
        if ($validator->fails()) {
            return response()->json([ 'error'=> $validator->errors() ]);
        }

        $old_password       = $request->old_password;
        $new_password       = $request->new_password;
        $confirm_password   = $request->confirm_password;
        
        
        if($new_password == $confirm_password){
            $current_password = Auth::user()->password;
            if(Hash::check($old_password, $current_password))
            {
                $id             = Auth::user()->id;
                $user           = User::findOrFail($id);
                $user->password = Hash::make($new_password);
                $user->save(); 
                return response()->json(['status'=>'Passowrd Updated!'], 200);
            }else{
                return response()->json([
                    'status' => 'Password Not Correct!',
                ], 404);
            }
        }else{
            return response()->json([
                'status' => 'New Password and Confirm password not matching!',
            ], 404);
        }
    }

    public function assignRole(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'role_id'      => 'required|array',
            'user_id'      => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([ 'error'=> $validator->errors() ]);
        }

        $user = User::find($request->user_id);
        if(!$user)
            return response()->json(['status' => 'User Not Found'], 404);
        $user->userRoles()->sync($request->role_id);
        return response()->json([
            'status' => 'Assign Success',
        ], 200);

    }
    
}
