<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Role;

class RolesController extends Controller
{
    public function index()
    {
        $roles = Role::latest()->paginate(25);
        return response()->json([
            'status' => 'Success',
            'roles'   => $roles
        ], 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required',
            'slug' => 'required|string|unique:roles',
        ]);
        if ($validator->fails()) {
            return response()->json([ 'error'=> $validator->errors() ]);
        }
        $data = $request->all();
        $data['slug'] = Str::slug($data['slug']);
        $role = Role::create($data);
        return response()->json([
            'status' => 'Success',
            'role'   => $role
        ], 201);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $data['slug'] = Str::slug($request->slug);

        $validator = Validator::make($data, [ 
            'name' => 'required',
            'slug' => 'required|unique:roles,slug,'.$request->role_id,
        ]);
        if ($validator->fails()) {
            return response()->json([ 'error'=> $validator->errors() ]);
        }
        $role = Role::find($request->role_id);
        if(!$role)
            return response()->json(['status' => 'Role Not Found'], 404);
        $role->name = $request->name;
        $role->slug = Str::slug($request->slug);
        $role->save();
        return response()->json([
            'status' => 'Success',
            'role'   => $role
        ], 201);
    }

    public function destroy($id)
    {
        $role = Role::find($id);
        if(!$role)
            return response()->json(['status' => 'Role Not Found'], 404);
        $role->delete();
        return response()->json([
            'status' => 'Deleted Success',
        ], 200);
    }
}
