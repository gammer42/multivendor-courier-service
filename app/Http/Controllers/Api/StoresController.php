<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\PickupPoint;
use Illuminate\Http\Request;
use App\Traits\UserTrait;
use App\Models\Role;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Validator;

use App\Models\Store;

class StoresController extends Controller
{
    use UserTrait;
    public function index()
    {
        $stores = Store::latest()->paginate(25);
        return response()->json([
            'status' => 'Success',
            'stores'   => $stores
        ], 200);
    }

    public function show($id)
    {
        $store = Store::find($id);
        if(!$store)
            return response()->json(['status' => 'Store Not Found'], 404);
        return response()->json([
            'status' => 'Success',
            'store'   => $store
        ], 200);
    }

    public function store(Request $request)
    {
        if(Auth::check()){
            if (Auth::user()->hasRole(array('operator', 'company'))) {
                $validator = Validator::make($request->all(), [
                    'shop_name'       => 'required|string',
                    'mobile'          => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
                    'address'         => 'required|string',
                    'name'            => 'required',
                    'email'           => 'required|email|unique:users',
                    'password'        => 'required|confirmed',
                    'phone'           => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
                    'title'           => 'required|string',
                    'contact_name'    => 'required|string',
                    'contact_phone'   => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
                    'contact_address' => 'required|string',
                    'google_place_id' => 'required|string',
                ]);
    
                if ($validator->fails()) {
                    return response()->json(['error' => $validator->errors()]);
                }
    
                $data = $request->all();
                $user = $this->userCreate($data);
                if (!$user)
                return response()->json(['status' => 'User Not Create'], 404);
    
                $store            = new Store();
                $store->shop_name = $request->shop_name;
                $store->mobile    = $request->mobile;
                $store->address   = $request->address;
                $store->save();
                if (!$store) {
                    $user->delete();
                    return response()->json(['status' => 'Store Not Create'], 404);
                } else {
                    Role::where('slug', 'store')->first()->userRoles()->sync($user->id);
                    $user->userStore()->sync($store->id);
    
                    $pickupPoint                  = new PickupPoint();
                    $pickupPoint->store_id        = $store->id;
                    $pickupPoint->title           = $request->title;
                    $pickupPoint->contact_name    = $request->contact_name;
                    $pickupPoint->contact_phone   = $request->contact_phone;
                    $pickupPoint->contact_address = $request->contact_address;
                    $pickupPoint->place_id        = Str::random(25);
                    $pickupPoint->google_place_id = $request->google_place_id;
                    $pickupPoint->save();
                    if (!$pickupPoint) {
                        return response()->json(['status' => 'Pickup Point Not Create'], 404);
                    } else {
                        
                    if (!Auth::user()->hasRole(array('store'))) {
                        $pickupPoint->assignStores()->sync(Auth::id(), ['is_company' => 0]);
                    }else{
                        $pickupPoint->assignStores()->sync($user->id, ['is_company' => 1]);
                    }
                        // $pickupPoint->assignStores()->sync(Auth::id(), ['is_company' => $is_company]);
    
                        return response()->json(
                            [
                                'status'      => 'Success',
                                'store'       => $store,
                                'pickupPoint' => $pickupPoint,
                                'user'        => $user
                            ],
                            201
                        );
                    }
                }
            }else{
    
                return response()->json(['error' => 'User are not authrized to create order'], 401);
            }
        }else{
            return response()->json(['error' => 'Unauthrized'], 401);
        }

        
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'shop_name' => 'required|string',
            'mobile'    => 'required|string',
            'address'   => 'required|string',
            'store_id'  => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([ 'error'=> $validator->errors() ]);
        }
        $store = Store::find($request->store_id);
        if(!$store)
            return response()->json(['status' => 'Store Not Found'], 404);
        $store->shop_name = $request->shop_name;
        $store->mobile    = $request->mobile;
        $store->address   = $request->address;
        $store->save();
        return response()->json([
            'status' => 'Success',
            'store'   => $store
        ], 200);
    }

    public function destroy($id)
    {
        $store = Store::with('userStore')->find($id);
        if(!$store)
            return response()->json(['status' => 'Store Not Found'], 404);
        $store->userStore()->delete();
        $store->userStore()->detach();
        $store->delete();
        return response()->json([
            'status' => 'Deleted Success',
        ], 200);
    }
}
