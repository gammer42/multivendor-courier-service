<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Traits\UserTrait;

use Illuminate\Support\Facades\Validator;
use App\Models\Team;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;

class TeamsController extends Controller
{
    use UserTrait;

    public function index()
    {
        $teams = Team::with('user', 'teamDrivers')->latest()->paginate(25);
        return response()->json([
            'status' => 'Success',
            'teams'  => $teams
        ]);
    }

    public function show($id)
    {
        $team = Team::with('user', 'teamDrivers')->find($id);
        if (!$team)
            return response()->json(['status' => 'Team Not Found'], 404);
        return response()->json([
            'status' => 'Success',
            'team'  => $team
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'team_name' => 'required|string',
            'driver_id' => 'required|array'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()]);
        }
        $team            = new Team();
        $team->user_id   = Auth::id();
        $team->team_name = $request->team_name;
        $team->save();
        if (!$team) {
            return response()->json(['status' => 'Team Not Create'], 404);
        } else {
            foreach ($request->driver_id as $key => $value) {
                $team->teamDrivers()->attach($value, []);
            }

            return response()->json([
                'status' => 'Success',
                'team' => $team
            ], 201);
        }
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'team_name' => 'required|string',
            'driver_id' => 'required|array',
            'team_id'   => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()]);
        }

        $team = Team::find($request->team_id);
        if (!$team)
            return response()->json(['status' => 'Team Not Found'], 404);
        $team->team_name = $request->team_name;
        $team->save();
        if (!$team) {
            return response()->json(['status' => 'Team Not Found'], 404);
        } else {
            $team->teamDrivers()->detach();
            foreach ($request->driver_id as $key => $value) {
                $team->teamDrivers()->attach($value, []);
            }
            return response()->json([
                'status' => 'Success',
                'team'   => $team
            ], 201);
        }
    }

    public function destroy($id)
    {
        $team = Team::find($id);
        if(!$team)
            return response()->json(['status' => 'Team Not Found'], 404);
        $team->teamDrivers()->detach();
        $team->delete();
        return response()->json([
            'status' => 'Deleted Success'
        ], 200);
    }
}
