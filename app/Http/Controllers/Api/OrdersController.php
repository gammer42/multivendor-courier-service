<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use App\Models\Order;
use App\Models\PickupPoint;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Carbon\Carbon;

use App\Models\User;

class OrdersController extends Controller
{

    public function index()
    {
        if (Auth::user()->hasRole(array('operator')) || Auth::user()->hasRole(array('company'))) {
            $orders = Auth::user()->orderOperators()->with('orderPickupPoints')->get();
            return response()->json([
                'status' => 'Success',
                'orders' => $orders
            ]);
        }else{
            return response()->json(['error' => 'User are not authrized to create order'], 401);
        }
    }

    public function show($id)
    {
        $order = Order::find($id);
        if(!$order)
            return response()->json(['status' => 'Order Not Found'], 404);
        return response()->json([
            'status' => 'Success',
            'order' => $order
        ], 200);
    }

    public function store(Request $request)
    {
        if (!Auth::user()->hasRole(array('store'))) {
            return response()->json(['error' => 'User are not authrized to create order'], 401);
        }
        $validator = Validator::make($request->all(), [
            'vehicle_type'     => 'required|string',
            'recipent_phone'   => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'recipent_name'    => 'required|string',
            'recipent_address' => 'required|string',
            'is_express'       => 'required|boolean',
            'pickup_point'    => 'required|integer',
           
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()]);
        }

        $order                    = new Order;
        $order->user_id           = Auth::id();
        $order->vehicle_type      = $request->vehicle_type;
        $order->recipent_phone    = $request->recipent_phone;
        $order->recipent_name     = $request->recipent_name;
        $order->is_express        = $request->is_express;
        $order->status_updated_at = Carbon::now()->format('Y-m-d h:i:s');
        $order->status            = 'created';
        $order->order_number      = Str::upper(Str::random(10));
        $order->save();

        if (!$order) {
            return response()->json(['status' => 'Order Not Create'], 404);
        } else {
            // foreach ($request->pickup_point as $key => $value) {
                $pickup_point = PickupPoint::find($request->pickup_point);
                $order->orderOperators()->attach($pickup_point->assignStores[0]->id);
                $order->orderPickupPoints()->attach($request->pickup_point);
                $order->orderStore()->attach($pickup_point->store_id);
            // }
            return response()->json([
                'status' => 'Success',
                'order' => $order
            ], 201);
        }
    }
}
