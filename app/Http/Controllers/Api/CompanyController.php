<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Traits\UserTrait;
use App\Models\Company;
use App\Models\Role;

class CompanyController extends Controller
{
    use UserTrait;
    public function index()
    {
        $companies = Company::with('user')->latest()->paginate(25);
        return response()->json([
            'status'    => 'Success',
            'companies' => $companies
        ], 200);
    }

    public function show($id)
    {
        $company = Company::with('user')->find($id);
        if (!$company)
            return response()->json(['status' => 'Company Not Found'], 404);
        return response()->json([
            'status'  => 'Success',
            'company' => $company
        ], 200);
    }

    public function store(Request $request)
    {
    //   dd($request->all());
        $validator = Validator::make($request->all(), [
            'company_name'         => 'required|string',
            'name'                 => 'required|string',
            'email'                => 'required|email|unique:users',
            'password'             => 'required|confirmed',
            'phone'                => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'owner_name'           => 'required|string',
            'time_zone'            => 'required|timezone',
            'cash_on_delivery_cap' => 'required|boolean',
            'currency'             => 'required|string',
            'delivery_commission'  => 'required|numeric',
            'express_commission'   => 'required|numeric',
            'cod_commission'       => 'required|numeric',
            'return_commission'    => 'required|numeric',
            'shipping_commission'  => 'required|numeric',
            'admin_commission'     => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()]);
        }

        $data = $request->all();
        $user = $this->userCreate($data);
        if (!$user) {
            return response()->json(['status' => 'User Not Create'], 404);
        } else {
            $company                          = new Company();
            $company->user_id                 = $user->id;
            $company->name                    = $request->company_name;
            $company->owner_name              = $request->owner_name;
            $company->time_zone               = $request->time_zone;
            $company->currency                = $request->currency;
            $company->delivery_commission     = $request->delivery_commission;
            $company->express_commission      = $request->express_commission;
            $company->cod_commission          = $request->cod_commission;
            $company->return_commission       = $request->return_commission;
            $company->shipping_commission     = $request->shipping_commission;
            $company->admin_commission        = $request->admin_commission;
            $company->cash_on_delivery_cap    = $request->cash_on_delivery_cap;
            $company->admin_payment_remaining = 0;
            $company->save();

            if ($user && $company) {
                Role::where('slug', 'company')->first()->userRoles()->sync($user->id);
                return response()->json([
                    'status'  => 'Success',
                    'company' => $company,
                    'user'    => $user
                ], 201);
            } else {
                $user->delete();
                return response()->json([
                    'status' => 'Failed',
                ], 403);
            }
        }
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id'           => 'required',
            'company_name'         => 'required|string',
            'owner_name'           => 'required|string',
            'time_zone'            => 'required|timezone',
            'cash_on_delivery_cap' => 'required|boolean',
            'currency'             => 'required|string',
            'delivery_commission'  => 'required|numeric',
            'express_commission'   => 'required|numeric',
            'cod_commission'       => 'required|numeric',
            'return_commission'    => 'required|numeric',
            'shipping_commission'  => 'required|numeric',
            'admin_commission'     => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()]);
        }

        $company = Company::find($request->company_id);
        if (!$company)
            return response()->json(['status' => 'Company not found'], 404);
        $company->name = $request->company_name;
        $company->owner_name = $request->owner_name;
        $company->time_zone = $request->time_zone;
        $company->cash_on_delivery_cap = $request->cash_on_delivery_cap;
        $company->currency = $request->currency;
        $company->delivery_commission = $request->delivery_commission;
        $company->express_commission = $request->express_commission;
        $company->cod_commission = $request->cod_commission;
        $company->return_commission = $request->return_commission;
        $company->shipping_commission = $request->shipping_commission;
        $company->admin_commission = $request->admin_commission;
        $company->save();
        return response()->json([
            'status'  => 'Success',
            'company' => $company,
        ], 201);

    }

    public function destroy($id)
    {
        $company = Company::with('user')->find($id);
        if(!$company)
            return response()->json(['status' => 'Company not found'], 404);
        $company->user->delete();
        $company->delete();
        return response()->json([
            'status' => 'Deleted Success',
        ], 200);
    }
}