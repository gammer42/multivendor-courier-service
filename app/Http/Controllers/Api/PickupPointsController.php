<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use App\Models\PickupPoint;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class PickupPointsController extends Controller
{
    public function index()
    {
        $pickup_points = PickupPoint::with('assignStores')->latest()->paginate(25);
        return response()->json([
            'status'        => 'Success',
            'pickup_points' => $pickup_points
        ], 200);
    }

    public function show($id)
    {
        $pickup_point = PickupPoint::find($id);
        if (!$pickup_point)
            return response()->json(['status' => 'Pickup Point Not Found'], 404);
        return response()->json([
            'status' => 'Success',
            'pickup_point' => $pickup_point
        ], 200);
    }

    public function store(Request $request)
    {
        // dd(!Auth::user()->hasRole(array('store')));
        if (Auth::user()->hasRole(array('operator', 'company', 'store'))) {
            $validator = Validator::make($request->all(), [
                'title'           => 'required|string',
                'contact_name'    => 'required|string',
                'contact_phone'   => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
                'contact_address' => 'required|string',
                'google_place_id' => 'required|string',
                'store_id'        => 'required|integer'
            ]);

            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()]);
            }

            $pickupPoint                  = new PickupPoint();
            $pickupPoint->store_id        = $request->store_id;
            $pickupPoint->title           = $request->title;
            $pickupPoint->contact_name    = $request->contact_name;
            $pickupPoint->contact_phone   = $request->contact_phone;
            $pickupPoint->contact_address = $request->contact_address;
            $pickupPoint->place_id        = Str::random(25);
            $pickupPoint->google_place_id = $request->google_place_id;
            $pickupPoint->save();
            if (!$pickupPoint) {
                return response()->json(['status' => 'Pickup Point Not Create'], 404);
            } else {
                if (!Auth::user()->hasRole(array('store'))) {
                    $pickupPoint->assignStores()->sync(Auth::id(), ['is_company' => Auth::user()->hasRole(array('company'))?1:0]);
                }else{
                    $user  =Auth::user()->userStore[0]->pickupPoint[0]->assignStores[0];
                    $pickupPoint->assignStores()->sync($user->id, ['is_company' => $user->hasRole(array('company'))?1:0]);
                }
                

                return response()->json(
                    [
                        'status'      => 'Success',
                        'pickupPoint' => $pickupPoint,
                    ],
                    201
                );
            }
        } else {

            return response()->json(['error' => 'User are not authrized to create order'], 401);
        }
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'           => 'required|string',
            'contact_name'    => 'required|string',
            'contact_phone'   => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'contact_address' => 'required|string',
            'google_place_id' => 'required|string',
            'pickup_point_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()]);
        }

        $pickup_point                         = PickupPoint::find($request->pickup_point_id);
        if (!$pickup_point)
            return response()->json(['status' => 'Pickup Point Not Found'], 404);
        $pickup_point->title                  = $request->title;
        $pickup_point->contact_name           = $request->contact_name;
        $pickup_point->contact_phone          = $request->contact_phone;
        $pickup_point->contact_address        = $request->contact_address;
        $pickup_point->google_place_id        = $request->google_place_id;
        $pickup_point->save();
        return response()->json([
            'status' => 'Success',
            'pickup_point' => $pickup_point
        ], 201);
    }

    public function destroy($id)
    {
        $pickup_point = PickupPoint::find($id);
        if (!$pickup_point)
            return response()->json(['status' => 'Pickup Point Not Found'], 404);
        $pickup_point->assignStores()->detach();
        $pickup_point->delete();
        return response()->json([
            'status' => 'Deleted Success'
        ], 200);
    }
}
