<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HasRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$roleName)
    {
        // dd($roleName);
        foreach (Auth::user()->userRoles()->get() as $role) {
            if (in_array($role->slug, $roleName)) {
                return $next($request);
            }
        }
        return response('Unauthorized.', 401);
    }
}
