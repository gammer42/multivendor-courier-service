<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PickupPoint extends Model
{
    use HasFactory, SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pickup_points';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['store_id', 'title', 'contact_name', 'contact_phone', 'contact_address', 'place_id', 'google_place_id'];

    public function assignStores()
    {
        return $this->belongsToMany(\App\Models\User::class, 'store_assign_to', 'pickup_point_id', 'user_id')->withPivot('is_company')->withTimestamps();
    }

    public function orderPickupPoints()
    {
        return $this->belongsToMany(\App\Models\Order::class, 'order_pickup_point', 'pickup_point_id', 'order_id');
    }

    public function store()
    {
        return $this->belongsTo(\App\Models\Store::class, 'store_id');
    }
}
