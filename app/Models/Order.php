<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['vehicle_type', 'recipent_phone', 'recipent_name', 'recipent_address', 'is_express', 'status', 'order_number', 'status_updated_at', 'driver_note', 'operator_note'];

    public function orderStore()
    {
        return $this->belongsToMany(\App\Models\Store::class, 'order_store', 'order_id', 'store_id');
    }

    public function orderPickupPoints()
    {
        return $this->belongsToMany(\App\Models\PickupPoint::class, 'order_pickup_point', 'order_id', 'pickup_point_id');
    }

    public function orderOperators()
    {
        return $this->belongsToMany(\App\Models\User::class, 'order_operator', 'order_id', 'operator_id')->withPivot('operator_note')->withTimestamps();
    }
}
