<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, SoftDeletes, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function userRoles()
    {
        return $this->belongsToMany(\App\Models\Role::class, 'role_user', 'user_id', 'role_id');
    }

    public function userStore()
    {
        return $this->belongsToMany(\App\Models\Store::class, 'user_store', 'user_id', 'store_id');
    }

    public function company()
    {
        return $this->hasOne(\App\Models\Company::class, 'user_id');
    }

    public function operators()
    {
        return $this->hasOne(\App\Models\Operator::class, 'user_id');
    }

    public function assignStores()
    {
        return $this->belongsToMany(\App\Models\PickupPoint::class, 'store_assign_to', 'user_id', 'pickup_point_id')->withPivot('is_company')->withTimestamps();
    }

    public function operatorUser()
    {
        return $this->belongsToMany(\App\Models\Operator::class, 'operator_user', 'user_id', 'operator_id')->withTimestamps();
    }

    public function drivers()
    {
        return $this->hasMany(\App\Models\Driver::class, 'user_id');
    }

    public function team()
    {
        return $this->hasMany(\App\Models\Team::class, 'user_id');
    }

    public function hasRole($roleName)
    {
        foreach ($this->userRoles()->get() as $role) {
            if (in_array($role->slug, $roleName)) {
                return true;
            }
        }

        return false;
    }

    public function orderOperators()
    {
        return $this->belongsToMany(\App\Models\Order::class, 'order_operator', 'operator_id', 'order_id')->withPivot('operator_note')->withTimestamps();
    }

}
