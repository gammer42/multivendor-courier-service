<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Operator extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'operators';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'company_id', 'name', 'city', 'country', 'time_zone','delivery_commission', 'express_commission', 'cod_commission', 'return_commission', 'shipping_commission'];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function company()
    {
        return $this->belongsTo(\App\Models\Company::class, 'company_id');
    }

    public function operatorUser()
    {
        return $this->belongsToMany(\App\Models\User::class, 'operator_user', 'operator_id', 'user_id')->withTimestamps();
    }

    public function operatorDriver()
    {
        return $this->belongsToMany(\App\Models\Driver::class, 'operator_driver', 'operator_id', 'driver_id');
    }
}
