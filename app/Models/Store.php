<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Store extends Model
{
    use HasFactory, SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'stores';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['shop_name', 'mobile', 'address'];

    public function pickupPoint()
    {
        return $this->hasMany(\App\Models\PickupPoint::class, 'store_id');
    }

    public function userStore()
    {
        return $this->belongsToMany(\App\Models\User::class, 'user_store', 'store_id', 'user_id');
    }

    public function orderStore()
    {
        return $this->belongsToMany(\App\Models\Order::class, 'order_store', 'store_id', 'order_id');
    }
}
