<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Driver extends Model
{
    use HasFactory, SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'drivers';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'vehicle_type', 'online_status', 'payment_type', 'commission_percent', 'salary', 'last_location'];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function operatorDriver()
    {
        return $this->belongsToMany(\App\Models\Operator::class, 'operator_driver', 'driver_id', 'operator_id');
    }

    public function companyDriver()
    {
        return $this->belongsToMany(\App\Models\Company::class, 'company_driver', 'driver_id', 'company_id');
    }

    public function teamDrivers()
    {
        return $this->belongsToMany(\App\Models\Team::class, 'driver_team', 'driver_id', 'team_id')->withTimestamps();
    }
}
