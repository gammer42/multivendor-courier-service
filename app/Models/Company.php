<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'companies';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'name', 'owner_name', 'time_zone', 'cash_on_delivery_cap', 'currency', 'delivery_commission', 'express_commission', 'cod_commission', 'return_commission', 'shipping_commission', 'admin_commission', 'admin_payment_remaining'];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function operators()
    {
        return $this->hasMany(\App\Models\Operator::class, 'company_id');
    }

    public function companyDriver()
    {
        return $this->belongsToMany(\App\Models\Driver::class, 'company_driver', 'company_id', 'driver_id');
    }
}
