<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\UsersController;
use App\Http\Controllers\Api\RolesController;
use App\Http\Controllers\Api\StoresController;
use App\Http\Controllers\Api\Auth\PasswordResetController;
use App\Http\Controllers\Api\CompanyController;
use App\Http\Controllers\Api\DriversController;
use App\Http\Controllers\Api\OperatorsController;
use App\Http\Controllers\Api\OrdersController;
use App\Http\Controllers\Api\PickupPointsController;
use App\Http\Controllers\Api\TeamsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [AuthController::class, 'login']);
Route::post('register', [AuthController::class, 'register']);

Route::group([
  'prefix' => 'password'
], function () {
  // Route::post('create', 'Api\Auth\PasswordResetController@create');
  // Route::get('find/{token}', 'Api\Auth\PasswordResetController@find');
  // Route::post('reset', 'Api\Auth\PasswordResetController@reset');

  Route::post('forgot', [PasswordResetController::class, 'forgot']);
  Route::get('check', [PasswordResetController::class, 'check']);
  Route::post('reset', [PasswordResetController::class, 'reset']);
});

Route::middleware('auth:api')->group(function(){

  //User
  Route::group([
    'prefix' => 'users'
  ], function () {
    Route::get('details', [AuthController::class, 'user_details']);
    Route::post('create', [UsersController::class, 'store']);
    Route::post('update', [UsersController::class, 'update']);
    Route::get('index', [UsersController::class, 'index']);
    Route::get('show/{id}', [UsersController::class, 'show']);
    Route::delete('delete/{id}', [UsersController::class, 'destroy']);
    Route::get('active/{id}', [UsersController::class, 'activeUser']);
    Route::get('inactive/{id}', [UsersController::class, 'inactiveUser']);
    Route::post('change-password', [UsersController::class, 'changePassword']);
    Route::post('role-assign', [UsersController::class, 'assignRole']);
  });

  Route::group([
    'prefix' => 'roles'
  ],function(){
    Route::post('create', [RolesController::class, 'store']);
    Route::post('update', [RolesController::class, 'update']);
    Route::get('index', [RolesController::class, 'index']);
    Route::delete('delete/{id}', [RolesController::class, 'destroy']);
  });

  Route::group(['prefix' => 'stores',
    'middleware' => 'role:store,operator,company',
  ], function(){
    Route::post('create', [StoresController::class, 'store']);
    Route::post('update', [StoresController::class, 'update']);
    Route::get('index', [StoresController::class, 'index']);
    Route::get('show/{id}', [StoresController::class, 'show']);
    Route::delete('delete/{id}', [StoresController::class, 'destroy']);
  });

  Route::group([
    'prefix' => 'companies'
  ], function () {
    Route::get('index', [CompanyController::class, 'index']);
    Route::get('show/{id}', [CompanyController::class, 'show']);
    Route::post('create', [CompanyController::class, 'store']);
    Route::post('update', [CompanyController::class, 'update']);
    Route::delete('delete/{id}', [CompanyController::class, 'destroy']);
  });

  Route::group([
    'prefix' => 'operators'
  ], function(){
    Route::get('index', [OperatorsController::class, 'index']);
    Route::get('show/{id}', [OperatorsController::class, 'show']);
    Route::post('create', [OperatorsController::class, 'store']);
    Route::post('update', [OperatorsController::class, 'update']);
    Route::delete('delete/{id}', [OperatorsController::class, 'destroy']);
  });

  Route::group([
    'prefix' => 'drivers'
  ], function () {
    Route::get('index', [DriversController::class, 'index']);
    Route::get('show/{id}', [DriversController::class, 'show']);
    Route::post('create', [DriversController::class, 'store']);
    Route::post('update', [DriversController::class, 'update']);
    Route::delete('delete/{id}', [DriversController::class, 'destroy']);
  });

  Route::group([
    'prefix' => 'teams'
  ], function () {
    Route::post('create', [TeamsController::class, 'store']);
    Route::post('update', [TeamsController::class, 'update']);
    Route::get('index', [TeamsController::class, 'index']);
    Route::get('show/{id}', [TeamsController::class, 'show']);
    Route::delete('delete/{id}', [TeamsController::class, 'destroy']);
  });

  Route::group([
    'prefix' => 'pickup-points'
  ], function () {
    Route::get('index', [PickupPointsController::class, 'index']);
    Route::get('show/{id}', [PickupPointsController::class, 'show']);
    Route::post('create', [PickupPointsController::class, 'store']);
    Route::post('update', [PickupPointsController::class, 'update']);
    Route::delete('delete/{id}', [PickupPointsController::class, 'destroy']);
  });

  Route::group([
    'prefix' => 'orders'
  ], function () {
    Route::post('create', [OrdersController::class, 'store']);
    Route::get('index', [OrdersController::class, 'index']);
    Route::get('show/{id}', [OrdersController::class, 'show']);
  });
  
});
